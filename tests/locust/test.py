# from locust import HttpLocust, TaskSet, task, between
from locust import HttpLocust, TaskSequence, seq_task, task, between

api_secret = 'testsecret1'


# class UserBehaviour(TaskSet):
class UserBehaviour(TaskSequence):
    @seq_task(1)
    @task(3)
    def index(self):
        self.client.get("/ok")

    @seq_task(2)
    @task(1)
    def error(self):
        self.client.get("/error")

    @seq_task(3)
    @task(1)
    def api_error(self):
        self.client.get("/api")

    @seq_task(4)
    @task(1)
    def api_ok(self):
        self.client.get("/api?API_SECRET={}".format(api_secret))

    @seq_task(5)
    @task(2)
    def api_heavy(self):
        self.client.get("/api/heavy")


class WebsiteUser(HttpLocust):
    task_set = UserBehaviour

    wait_time = between(1, 3)
