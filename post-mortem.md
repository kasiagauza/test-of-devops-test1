# Post mortem

## General information

Tasks done:

* Manual AWS EC2 configuration
* Automation part (preparation of Ansible role and a successful run of the role on the EC2 instance)

Tasks unfinished:

* Locust load tests (*no specific logs included*)
* CI explanation (*only uncomplete description*)

Anyway - finished or not - I would you like to go through all the parts below.

## Table of Contents

- [Post mortem](#post-mortem)
  - [General information](#general-information)
  - [Table of Contents](#table-of-contents)
  - [PART 1: setup](#part-1-setup)
  - [PART 2: automation](#part-2-automation)
    - [Preface](#preface)
    - [The Ansible role structure](#the-ansible-role-structure)
    - [Public_keys and source code location](#publickeys-and-source-code-location)
    - [Variables](#variables)
    - [Secrets considerations](#secrets-considerations)
    - [Monitoring considerations](#monitoring-considerations)
    - [Docker app info and fixes](#docker-app-info-and-fixes)
  - [PART 3: test](#part-3-test)
  - [PART 4: CI pipeline](#part-4-ci-pipeline)

## PART 1: setup

Manual steps:

* Launched the AWS Debian Stretch instance from EC2 console (*Instances > Launch Instance*)
* Created the *admin* user's SSH keypair
* Login to the EC2 instance w/ the *admin* account and the SSH keys
  * `1. eval $(ssh-agent), 2. ssh-add ~/.ssh/aws-testsshkeys.pem, 3. ssh admin@aws-ec2-instance-IP`
* Created a *testuser* group for all the developers and *tuser* non-root account
  * `sudo addgroup testuser`
* Created four local users, starting from *kkokoszka*, and added them to *testuser* and *root* groups (and later - to the *docker* group); example below:
  * `sudo adduser kkokoszka`
  * `sudo adduser kkokoszka testuser`
  * `sudo adduser kkokoszka root`
* Enabled passwordless sudo for the users, like:
  * `sudo -s --> echo 'kkokoszka ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/`
* Copied public keys the EC2 instance using *admin* account
  * `scp -r public_keys/ admin@aws-ec2-instance-IP:/tmp/public_keys/`
* Added SSH keys to the .ssh directories of the developers. Example below:
  * `mkdir .ssh`
  * `cat /tmp/public_keys/krzysztof.kokoszka > .ssh/authorized_keys`
* Added non-root user *tuser* accessible by user and password
  * `sudo adduser tuser --> password: testuser1`
  * `sudo adduser tuser testuser`
  * `sudo adduser tuser docker`
  * `sudo -s --> echo 'tuser ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/`
* Enabled login to the instance w/ the password
  * `vim /etc/ssh/sshd_config --> PasswordAuthentication yes`
  * `sudo systemctl restart ssh`
* Tested login to the instance w/ the non-root *tuser*
  * `ssh tuser@aws-ec2-instance-IP`
* Installed needed apps
  * `sudo apt install -y git python-pip`
* Installed *docker-ce* package
  * `sudo curl -sSL https://get.docker.com/ | sh`
* Added all developers to the *docker* group, example:
  * `sudo adduser kkokoszka docker`
* Cloned the *test-of-devops-test1* repo
  * `git clone https://kasiagauza@bitbucket.org/kasiagauza/test-of-devops-test1.git`
* Added *gunicorn* package to the requirements file, otherwise the Docker container will fail w/ the *gunicorn not found* error
  * `echo 'gunicorn' >> src/requirements.txt`
* Built the Docker image in src/ directory
  * `sudo docker build .`
* Started the container
  * `docker run -dit -e PORT=7777 -e API_SECRET=testpassword1 -p 8080:7777 --name flaskapp <image-id>`
* Enabled the EC2 instance inbound/outbound HTTP and TCP traffic
  * ec2 > security group > enable inbound/outbound HTTP/TCP traffic
* Tested the app from my laptop
  * `curl 18.156.3.214:8080/ok --> it's ok`
* Saved the container logs to a file accessible by *tuser*
  * `docker logs -f flask_node &> /tmp/docker.log`
  * `sudo chown :testuser /tmp/docker.log`
* Ensure the system is up to date
  * `sudo apt update && sudo apt upgrade -y`
* Bonus: tested the infrastructure setup Ansible role on the EC2 Debian instance
  * `ansible-playbook infra_setup.yml -i inventory --limit 'awsdebian' -K -v --> works`

## PART 2: automation

### Preface

I chose Ansible to automate the infrastructure setup on the EC2 Debian node.

I prepared a role and tested it on AWS Debian Stretch and on Vagrant Ubuntu 18 VM.

Ansible is easy to set up, as it relies only on SSH connection + Python installed on the remote node (*to be precise: it only needs SSH connection, as Python can be installed with the 'raw' module*). It doesn't require to set up an agent.

I avoided using run commands/modules, such as *command, raw* or *shell*. It's a common anti-pattern to use Ansible just as a tool to execute shell steps - and this could break playbook idempotency.

### The Ansible role structure

* The role is in *test-of-devops-test1/cm/roles/* directory
* The *(...)/flask_node/README.md* file lists all the details, including environment variables explanation

**Important**: To run the role you need Ansible >= 2.9. The version added *mounts* directive to the 'docker_container' module. I use it to set up a *bind mount* between the host and the container. It allows to update the Python app w/o the need to rebuild the image. I decided to use *bind mount* because it's usually the fastest option to set up and it's suited well for small development environments.

The Ansible *flask_node* role was created with *ansible-galaxy* command:

* `ansible-galaxy init flask_node`

The *init* directive sets up a complete role structure and *.travis.yml* CI file, if needed.

The role structure was tested w/ *ansible-lint*. It should be additionaly tested with Molecule/TestInfra to be sure of the infrastructure state, but it was not the scope of the task. Testing the role on Ubuntu 18 VM was enough before running it on AWS EC2 instance. Additionaly, I've checked that the flask app container survives the restart of the instance.

The Ansible *flask_node* role has three main tasks:

* **host_update.yml**- updates the host
* **create_env.yml** - configures the environment
* **app_deployment.yml** - deploys the app

The *infra_setup.yml* playbook runs all the task plus two other additional roles:

* geerlingguy.pip --> installs python pip plus selected packages (default: docker)
* geerlingguy.docker --> installs Docker app (community edition)

This enables the host to build Docker images and manage Docker containers.

Those roles are downloaded from *ansible galaxy* repository and can be installed by hand:

* ansible-galaxy install geerlingguy.pip
* ansible-galaxy install geerlingguy.docker

The *flask_node* role has four *tags*:

* **update**:
  * updates the host system packages and reboots the OS, if needed
* **install_and_create_env**:
  * creates test user group
  * creates user accounts w/ SSH keys and adds sudoers files
  * installs Python Docker package and Docker app/daemon
* **deploy**:
  * builds the Docker image from Dockerfile
  * sets bind mount from the scripts directory
  * runs the container
  * the 'deploy' tag by default triggers the 'rerun_container' tag explained below
* **rerun_container** - runs or reruns the container :)

### Public_keys and source code location

I've copied *public_keys* and *src* directories to *roles/flask_node* directory to avoid using relative paths. Files and variables in the default role directories are discovered automatically.

*Dockerfile* contains undefined *$PORT*, so it could be treated as a template and placed in the role *templates* folder. Now the variable is set on *docker_container* module execution - that's good enough.

### Variables

Variables from the *default* role directory have almost the lowest priority, so they can be easily overwritten in playbooks or with *--extra-vars* flag during playbook execution, e.g.

`ansible-playbook (...) --extra-vars "app_port=6543" app_api_secret=!SEcretsecret9`

Example variables:

* user_names - the dictionary of user names and logins
* app_port - the container flask app port
* host_port - the host port to which the container app port is mapped to

**Notice**: I needed to set a separate logins, because the user names contains dot '.' character - it is not accepted by Linux NAME_REGEX configuration variable.

### Secrets considerations

The role variables are set in *default/main.yml* file. The file contains, among all, application port and api secret. The secret is in plain text and of course it should be encrypted. The easiest solution is to use **Ansible Vault**. It's a good tool for a personal use - we can encrypt a variable or a whole file, but the Vault is rather limited when it comes to work with a larger team. As far as I know, it doesn't quite address the secure introduction problem. Better solution could be a standalone **Hashicorp Vault** server that offers a good secrets and credentials management option. It can be integrated with *Ansible Tower/AWX* and *AWS*. I haven't tested **AWS Secrets Manager** or **Parameter Store**, unfortunately.

### Monitoring considerations

AWS by default enables basic CloudWatch monitoring per EC2 instance. It monitors CPU utilization, disk I/O and network traffic. Allows to set alarms and prepare statistics, but it's main disadvantage is that it's bound to AWS. It may be better to have a cloud agnostic monitoring tool, such as Prometheus - which btw. can have a Docker daemon configured as a target.

### Docker app info and fixes

* **requirements.txt** had to be updated, as the **gunicorn** package was missing. I have no idea which exact *gunicorn* version should be used, so I added the latest one (works):
  * `echo 'gunicorn' >> src/requirements.txt`
* The container is using default **bridged** networking and I **publish** the Docker app port to the host (default port mapping: 7000:7777)
* Docker container has a **healthcheck** included in the form of the following script:
  * `curl -f http://localhost:{{ app_port }}/ok || exit 1`
* Docker container uses *bind mount* that links the host flask app source directory with the *usr/src/app/* directory in the container. It allows to update Python source files and rerun the app without the need to rebuild the image
* I added *.dockerignore* to block some files and directories from being copied to the container

## PART 3: test

I've tried to use *locust* library. I've prepared a test and ran it in *step load* mode, but I have trouble interpreting the results. I have almost none experience with the load tests.

I attached the test file: *testst/locust/test.py*

How I executed the test:

* `locust -f test.py --step-load`
* I've entered the web interface and tested the app with different number of users: 1, 10, 50. Settings: Hatch rate = 1, Number of users to increase by step: 3, Step duration: 1s or 7s. If the number of user was higher than 10, the fail rate went up, especially with the *heavy* part. The common error: *ConnectionError(ProtocolError('Connection aborted.', RemoteDisconnected('Remote end closed connection without response')))*.

## PART 4: CI pipeline

I am the most familiar with the Jenkins tool and I have used it before to deploy Docker apps, though not to the AWS EC2 instances.

Jenkins may be hosted on our bare metal/VM etc. or running on AWS EC2 instance and use other EC2 instance as a build/test node.

Prerequisites:

* Jenkins plugins, among all:
  * Bitbucket
  * Docker Pipeline

Workflow:

* Set up a *webhook* in the Bitbucket repository that triggers Jenkins pipeline on a code commit. Another option is to have Jenkins monitor the repository CHANGES with *Poll SCM*.
  * `repository --> settings --> webhooks --> add new webhook`
* Jenkins pipeline clones the repository (*notice: the whole pipeline is in version controlled Jenkinsfile*)
* Conduct static code analysis (*integration w/ SonarQube?*), use *pylint* etc.
* Run unit tests that produce *xUnit* report that Jenkins can interpret
* If the tests above succeed, Jenkins builds the Docker image:
  * `image = docker.build(...)`
  * As a side note, I could use just *sh* shell step to run *docker build* and *docker run* steps in separate stages. *docker run* could be wrapped into *withEnv* directive that sets the *PORT* variable.
* If Docker container is running, conduct tests (*API testing?*)
* If everything is ok, accept the commit(s)/code changes
* Deploy the Docker container on the AWS EC2 instance

I presume that Bitbucket itself has an option to create a neat pipeline to build and test the app, and publish it to AWS (*with the CodeDeploy support?*).
