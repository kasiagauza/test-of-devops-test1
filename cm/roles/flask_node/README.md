flask_node
=========

The role prepares the host environment for the flask app container deployment and runs the app.

Requirements
------------

Ansible >= **2.9**

Dependencies
------------

Roles:

* geerlingguy.pip
* geerlingguy.docker

Steps to install the roles:

    ansible-galaxy install geerlingguy.pip
    ansible-galaxy install geerlingguy.docker

Tags
--------------

* **update**:
  * updates the host system packages and reboots the OS, if needed
* **install_and_create_env**:
  * creates test user group
  * creates user accounts w/ SSH keys and adds sudoers files
  * installs Python Docker package and Docker app/daemon
* **deploy**:
  * builds the Docker image from Dockerfile
  * sets bind mount from the scripts directory
  * runs the container.
  * the 'deploy' tag by default triggers the 'rerun_container' tag explained below
* **rerun_container** - runs or reruns the container :)

Role Variables
--------------

Location:

    defaults/main.yml

User settings:

* **user_names** - the dictionary of user names and logins
* **user_group** - the group for all developers (default: testuser)

App configuration:

* **host_port** - the host port to which the container app port is mapped to (default: '7000')
* **app_port** - the container flask app port (default: '7777')
* **app_api_secret** - (default: 'testsecret1')
* **app_source_dir** - the app source directory on the Ansible controller, that will be copied to the host (default: 'src/')
* **app_host_dir** - the app source directory on the host, e.g. EC2 instance (default: '/opt/flask_app_src/')
* **app_container_dir** - the app source directory on the Docker container (default: '/usr/src/app/'; change with caution, because the path is hardcoded in the Dockerfile)
* **app_image_name** - the Docker image name (default: 'flask_test_image')
* **app_container_name** - the Docker container name (default: 'flask_test')

The variables can be easily overwitten in plays or with *--extra-vars* flag during *ansible-playbook* execution.

Example Playbook
----------------

**Important:** 'pip' and 'docker' roles need to be installed.

Example 'infra_setup.yml':

    - hosts: ubuntu
      become: yes
      remote_user: vagrant

      vars:
        pip_install_packages:
        - name: docker

      roles:
        - geerlingguy.pip
        - geerlingguy.docker
        - flask_node
  
Run command:

    ansible-playbook infra_setup.yml -i inventory --limit 'vagubuntu' -K -v

The complete playbook location:

    cm/roles/flask_node/infra_setup.yml

**Notice:** The *infra_setup* playbook inventory reflects the current AWS EC2 instance IP, which keeps changing. It also points to a specific AWS SSH key residing on my laptop (*ansible_ssh_private_key_file* variable).

Author Information
------------------

Kasia Gauza <nimrod7@gmail.com>
